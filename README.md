# Proyecto Maleteo

Ejercicio de maquetación, envio de formulario, y validación del mismo. 

Se puede encontrar un deploy del proyecto en el siguiente enlace: **[https://juanpabloamador.gitlab.io/proyecto-maleteo](https://juanpabloamador.gitlab.io/proyecto-maleteo)**.

El deploy hace uso del servicio Gitlab Pages, mediante el archivo de configuración **.gitlab-ci.yml**

Proyecto realizado con **html**, **css**, y **javascript** básico.

La maquetación fue a partir de un diseño previo que nos fue suministrado, 
junto con las imágenes del mismo. 

Se utilizo **flexbox** para la maquetación y varios estilos CCS, y se agregaron media querys para adaptar la landing a distintos tamaños de pantalla.



